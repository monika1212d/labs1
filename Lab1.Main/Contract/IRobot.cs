﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    public interface IRobot
    {
        string metoda1();
        string metoda2();
        string poskladajCzesci();
    }
}
