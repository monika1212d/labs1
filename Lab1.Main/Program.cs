﻿using System;
using Lab1.Contract;
using Lab1.Implementation;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        public ICollection<ISsak> StworzKolekcje()
        {
            List<ISsak> kolekcja = new List<ISsak>{};
            kolekcja.Add(new Impl1());
            kolekcja.Add(new Impl2());
            kolekcja.Add(new Impl2());
            kolekcja.Add(new Impl1());
            kolekcja.Add(new Impl1());
            return kolekcja;
        }

        public void KKolekcji(ICollection<ISsak> kolekcja)
        {
            foreach (var item in kolekcja)
            {
                item.metoda1();
            }
        }
      
        static void Main(string[] args)
        {
        }
    }
}
